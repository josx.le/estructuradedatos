/*-------------------------------------------------------------------------------------------------- */

// Alumno: López Espinoza José

// Fecha: 17 de Marzo del 2024

// Cuatrimestre y Grupo: 4C

/*-------------------------------------------------------------------------------------------------- */

import java.util.*;
import lista_enlazada.*;

public class App {
    Lista pila = new Lista();

    public static void main(String[] args) {
        App app = new App();
        app.Menu();
    }

    public void Menu() {
        Scanner leer = new Scanner(System.in);
        int opc;

        do {
            clear();
            mostrarMenu();

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Ingresa un número válido ");
                leer.nextLine();
                mostrarMenu();
            }

            opc = leer.nextInt();
            leer.nextLine();

            switch (opc) {
                case 1:
                    clear();
                    agregarElemento(leer);
                    break;
                case 2:
                    clear();
                    eliminarElemento(leer);
                    break;
                case 3:
                    clear();
                    consultarUltimoElemento(leer);
                    break;
                case 4:
                    clear();
                    verificarPilaVacia(leer);
                    break;
                case 5:
                    clear();
                    conocerCantidadElementos(leer);
                    break;
                case 6:
                    clear();
                    desplegarElementos(leer);
                    break;
                case 7:
                    clear();
                    System.out.println("Bye..");
                    break;
                default:
                System.out.println("ERROR! Ingresa un número válido ");
            }

        } while (opc != 7);

        leer.close();
    }

    public void agregarElemento(Scanner leer) {
        char respuesta;
        do{
        System.out.print("Ingrese el elemento a agregar: ");

        while (!leer.hasNextInt()) {
            clear();
            System.out.println("ERROR! Solo se aceptan números.");
            leer.nextLine();
            System.out.print("\nIngrese valor al final de la lista: ");
        }

        int elemento = leer.nextInt();
        pila.insertarAlFrente(elemento);
        pila.imprimir();

        System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void eliminarElemento(Scanner leer) {
        char respuesta;
        do{
        try {
            Object elementoEliminado = pila.eliminarDelFrente();
            System.out.println("Elemento eliminado: " + elementoEliminado);
            pila.imprimir();
        } catch (ExxeptionListaVacia e) {
            System.out.println("ERROR! La pila está vacía, no se puede eliminar ningún elemento.");
        }

        System.out.print("\n¿Deseas eliminar otro valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas Eliminar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');
    }

    public void consultarUltimoElemento(Scanner leer) {
        char respuesta;
        do{

        if (!pila.estaVacia()) {
            System.out.println("El último elemento de la pila es: " + pila.primerNodo.getDato());
        } else {
            System.out.println("La pila está vacía, no hay elementos para consultar.");
        }

        System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void verificarPilaVacia(Scanner leer) {
        char respuesta;
        do{
        if (pila.estaVacia()) {
            System.out.println("La pila está vacía.");
        } else {
            System.out.println("La pila no está vacía:");
            pila.imprimir();
        }

        System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void conocerCantidadElementos(Scanner leer) {
        char respuesta;
        do{    
        System.out.println("La cantidad de elementos en la pila es de: " + pila.contarElementos());

        System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');
    }

    public void desplegarElementos(Scanner leer) {
        char respuesta;
        do{

            pila.imprimir();

            System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');
    }

    public static void mostrarMenu() {
        System.out.println("\nLista de opciones: ");
        System.out.println("--------------------------------------");
        System.out.println("1. Agregar elemento");
        System.out.println("2. Eliminar elemento");
        System.out.println("3. Consultar el último elemento");
        System.out.println("4. Saber si la lista esta vacía");
        System.out.println("5. Cantidad de elementos");
        System.out.println("6. Desplegar todos los elementos");
        System.out.println("7. Salir");
        System.out.print("\nSeleccione una opción: ");
    }

    public static void clear() {

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
