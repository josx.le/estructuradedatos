package lista_enlazada;

public class ExxeptionListaVacia extends RuntimeException {
    
    // Constructor sin argumentos

    public ExxeptionListaVacia() {
        this("Lista");
    }

    // Constructor con un argumento

    public ExxeptionListaVacia(String nombre) {
        super("\nLa lista " + nombre  +" esta vacía.");
    }

}// ExxeptionListaVacia
