package lista_enlazada;

public class Lista {
    public Nodo primerNodo;
    public Nodo ultimoNodo;
    public String nombre;

    // Se crea el constructor con la lista 

    public Lista() {
        nombre = "Lista";
    }

    public Lista(String nombreLista) {
        nombre = nombreLista;
        primerNodo = ultimoNodo = null;
    }

    // Determinar si la lista está vacía 

    public boolean estaVacia() {
        return primerNodo == null;
    }

    // Imprimir

    public void imprimir() {
        if(estaVacia()) {
            System.out.println("\nPila vacía, nada que imprimir");
        }
        else{
            System.out.print("La lista es: ");
            Nodo actual = primerNodo;

            // Mientras no sea el final de la lista, se imprime los datos del nodo actual

            while (actual != null) {
                System.out.printf("%s ", actual.dato);
                actual = actual.enlace;
            }
            System.out.println("");
        }
    } 

    // InsertarAlFrente

    public void insertarAlFrente(Object dato) {
        if(estaVacia()) {
            primerNodo = ultimoNodo = new Nodo(dato);
        }
        else{
            primerNodo = new Nodo(dato, primerNodo);
        }
    }

    // Eliminar el primer elémento

    public Object eliminarDelFrente() throws ExxeptionListaVacia {
        if(estaVacia()) {
            throw new ExxeptionListaVacia(nombre);
        }

        Object elementoEliminado = primerNodo.dato;

        if(primerNodo == ultimoNodo) {
            primerNodo = ultimoNodo = null;
        }
        else {
            primerNodo = primerNodo.enlace;
        }
        return elementoEliminado;

    }

    // Contar elementos

    public int contarElementos() {
        int cont = 0;
        Nodo indice;
        for(indice = primerNodo; indice != null; indice = indice.enlace)
        cont++;
        return cont;
    }

    // Obtener la cantidad de elementos de la lista

    public int obtenerCantidadElementos() {
        return contarElementos();
    }

}// Lista
