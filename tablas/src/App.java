import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner leer = new Scanner(System.in);
        int filas = 3;
        int columnas = 4;
        int[][] numeros = new int[filas][columnas];
        int [][] nums = { {4, 8, 12},
                          {2, 26, 74},
                          {3, 16, 20},
                          {15, 35, 14}
                        };

        //Imprime tablas

        System.out.println("Trabajando con tablas\n");

        System.out.printf("%18s %9s %9s", "col 1", "col 2", "col 3");
        System.out.println("");
        for(int i = 0; i < 4; i++) {
            System.out.print("Fila " + (i + 1) + ":");
            for(int j = 0; j < 3; j++) {
                System.out.printf("%10s" , nums[i][j]);
            }
            System.out.println("");
        }

        //Ingresa Tablas
        
        System.out.println("Ingresa los valores de la tabla: ");
        
        for(int i = 0; i < filas; i++){
            for (int j = 0; j< columnas; j++) {
                System.out.print(i + "," + j + ": ");
                numeros [i][j] = leer.nextInt();
            }
            System.out.println("");
        }

        leer.close();
        
    }//Main

}//Class
