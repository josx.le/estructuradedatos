package lista;

public class Candidatos {
    private String nombre;
    private String puesto;

    /*Setters */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    /*Getters */

    public String getNombre() {
        return nombre;
    }

    public String getPuesto() {
        return puesto;
    }

}
