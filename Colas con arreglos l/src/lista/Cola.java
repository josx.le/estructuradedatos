package lista;

public class Cola {
    int max;
    int inicio = 0;
    int fin = 0;
    Candidatos[] candidatos;



    public Cola(int max) {
        this.max = max;
        candidatos = new Candidatos[max];
        
        for(int i = 0; i < candidatos.length; i++) {
            candidatos[i] = new Candidatos();
        }
    }

    public boolean estaLlena() {
        return fin == max;
    }

    public boolean estaVacio() {
        return inicio == fin;
    }

    public void agregar(String nombre, String puesto) {
        candidatos[fin].setNombre(nombre);
        candidatos[fin].setPuesto(puesto);
        fin++;
    }

    public void retirar() {
        if(estaVacio()) {
            System.out.println("Ya no hay candidatos para retirar.");
        }
        else {
            System.out.println("Removiendo el registro");
            for(int i = 0; i < fin - 1; i++) {
                candidatos[i].setNombre(candidatos[i + 1].getNombre());
                candidatos[i].setPuesto(candidatos[i + 1].getPuesto());
            }
            fin--;
            System.out.println("El registro se ha eliminado.");
        }
    }

    public void imprimir() {
        if(estaVacio()) {
            System.out.println("Ya no hay candidatos para mostrar.");
        }
        else {
            System.out.println("\nNúmero \tNombre \t\tPuesto");
            System.out.println("--------------------------------------------------");
            for(int i = 0; i < fin; i++) {
                System.out.print(i + 1);
                System.out.print("\n" + candidatos[i].getNombre());
                System.out.println("\t\t" + candidatos[i].getPuesto());
            }
        }
    }

    public void verPrimero() {
        if(estaVacio()) {
            System.out.println("Ya no hay candidatos esperando.");
        }
        else {
            System.out.println("Siguiente candidato");
            System.out.println("Nombre: " + candidatos[inicio].getNombre());
            System.out.println("Puesto: " + candidatos[inicio].getPuesto());
        }
    }
}
