import java.util.Scanner;
import lista.*;

public class App {

    static Scanner read = new Scanner(System.in);
    static Scanner continuar = new Scanner(System.in);
    static Cola filaCandidatos;
    public static void main(String[] args) throws Exception {

        int numCandidatos, opcion;

        clear();
        System.out.println("Candidatos para entrevista!");
        System.out.print("\nNúmero de candidatos: ");
        numCandidatos = read.nextInt();
        filaCandidatos = new Cola(numCandidatos);
        
        do {
            opcion = menu();
            switch (opcion) {
                case 1:
                registrar();
                    break;
                case 2:
                retirar();
                    break;
                case 3:
                siguienteCandidato();
                    break;
                case 4:
                imprimir();
                    break;
            }
        }while(opcion != 5);

        read.close();
        continuar.close();
    }

    public static void imprimir() {
        clear();
        System.out.println("Lista de candidatos\n");
        filaCandidatos.imprimir();
        enter();
    }

    public static void siguienteCandidato() {
        clear();
        filaCandidatos.verPrimero();
        enter();
    }

    public static void retirar() {
        clear();
        System.out.println("Remover registro\n");
        filaCandidatos.verPrimero();
        filaCandidatos.retirar();
        enter();
    }

    public static void registrar() {
        String nombre, puesto;
        clear();
        if(filaCandidatos.estaLlena()) {
            System.out.println("Registros completos, no se pueden agregar más.");
        }
        else {
            System.out.println("Registro de candidatos");
            System.out.print("Nombre: ");
            nombre = read.next();
            System.out.print("Puesto: ");
            puesto = read.next();
            filaCandidatos.agregar(nombre, puesto);
            filaCandidatos.imprimir();
        }
        enter();
    }

    public static int menu() {

        int opcion;

        do {
            clear();
            System.out.println("Sistema de candidatos");
            System.out.println("1. Registrar");
            System.out.println("2. Retirar");
            System.out.println("3. Siguiente candidato");
            System.out.println("4. Consultar todos los candidatos");
            System.out.println("5. Salir");
            System.out.print("\nOpción: ");

            opcion = read.nextInt();

        }while(opcion < 1 || opcion > 5);

        return opcion;
    }

    public static void clear() {

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void enter() {

        continuar.useDelimiter("\n");
        System.out.print("\nPresione ENTER para continuar...");
        continuar.nextLine();

    }
}