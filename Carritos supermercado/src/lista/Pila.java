package lista;

public class Pila {
    int max = 20;
    int cima = 0;
    Carritos[] carritos = new Carritos[max];


    // Iniciarlizar el arreglo de objetos

    public Pila() {
        for(int i = 0; i < max; i++) {
            carritos[i] = new Carritos();
        }
    }

    // Saber si la pila esta llena 

    public boolean pilaLlena() {
        return cima >= max;
    }

    // Saber si la pila esta vacía 

    public boolean pilaVacia() {
        return cima == 0;
    }

    // Cantidad de elementos agregados

    public int numElementos() {
        return cima;
    }

    // Cantidad máxima de elementos que se pueden agregar

    public int maxElementos() {
        return max;
    }

    // Agregar carrito a la pila

    public void agregar(String codigo) {
        if(pilaLlena()) {
            System.out.println("No se pueden agregar mas carritos.");
        }
        else {
            carritos[cima] = new Carritos();
            carritos[cima].setCodigo(codigo);
            cima++;
        }
    }

   // Retirar un carrito de la cima

    public int retirar() {
        int posicion = -1;

        if(pilaVacia()) {
            System.out.println("No hay carritos para sacar.");
        }
        else{
            cima --;
            System.out.println("Carrito retirado con el código: " + carritos[cima].getCodigo() + "\n");
            posicion = cima;

            carritos[cima].setCodigo(null);
        }
        return posicion;
    }
    
    // Imprimir la lista de carritos

    public void imprimir() {
        if(pilaVacia()) {
            System.out.println("No hay carritos que imprimir.");
        }
        else {
            System.out.println("\nCódigos");
            System.out.println("------------------------------------------------------");
            for(int i = 0; i < cima; i++) {
                System.out.println(carritos[i].getCodigo());
            }
        }
    }

    public int posicionn() {
        if (cima == 0) {
            return -1;
        } else {
            return cima - 1;
        }
    } 
}


