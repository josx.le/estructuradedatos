/*--Integrantes--*/

/* 
 * Luna Flores Yamileth Guadalupe 
 * García Lara America Denisse
 * López Espinoza José
*/

/*  Practica 5 */

/*  Carritos del supermercado
    Un pequeño supermercado de recién
    apertura, cuenta con pocos carritos para
    la despensa, y para tener un mejor
    control de ellos, realizará un sistema que
    registre las entradas y salidas de cada
    carrito.

    Solo se cuenta con 20 carritos, que están identificados
    con un código y se colocan en fila, conforme se van
    registrando en el sistema. De los 20 carritos se forman 3
    filas, una de 10 para carros normales, otra de 5 para
    carros que tienen portabebés y otra de 5 para carros
    pequeños. */

import java.util.*;
import lista.*;

public class App {

    static Scanner leer = new Scanner(System.in);
    static Scanner continuar = new Scanner(System.in);

    static Pila CarritoNormal = new Pila();
    static Pila CarritoPortaBebes = new Pila();
    static Pila CarritoPequeno = new Pila();

    public static void main(String[] args) throws Exception {     
        
        int opcionTipoCarrito, opcionGestion;

        do {
            opcionTipoCarrito = menuTipoCarrito();

            if(opcionTipoCarrito != 4) {
                do {

                    opcionGestion = menuGestion();

                    switch (opcionGestion) {
                        case 1:
                            ingresarCarrito(opcionTipoCarrito);
                            break;
                        case 2:
                            retirarCarrito(opcionTipoCarrito);
                            break;
                        case 3:
                            carritosTotales(opcionTipoCarrito);
                            break;
                        case 4:
                            clear();
                            System.out.println("Bye...");
                            break;
                        default:
                            System.out.println("Opción inválida.");
                    }

                } while (opcionGestion != 4);
            }

        } while (opcionTipoCarrito != 4);
        
        leer.close();
        continuar.close();

    } // Main

    public static void carritosTotales(int tipoCarrito) {
        clear();
        System.out.println("--Total de carritos disponibles--\n");
        if (tipoCarrito == 1) {
            System.out.println("Cantidad de carritos normales: " + CarritoNormal.numElementos());
            CarritoNormal.imprimir(); 
        } else if (tipoCarrito == 2) {
            System.out.println("Cantidad de carritos con portabebés: " + CarritoPortaBebes.numElementos());
            CarritoPortaBebes.imprimir(); 
        } else if (tipoCarrito == 3) {
            System.out.println("Cantidad de carritos pequeños: " + CarritoPequeno.numElementos());
            CarritoPequeno.imprimir(); 
        }
        System.out.println("------------------------------------------------------\n");
        Enter();
    }
       

    public static void retirarCarrito(int tipoCarrito) {
        int posicion = -1;
        char respuesta;

        do{
            clear();
            if (tipoCarrito == 1) {

                System.out.println("Retirar el último carrito normal");
                System.out.println("----------------------------------------\n");

                posicion = CarritoNormal.retirar();
            } else if (tipoCarrito == 2) {

                System.out.println("Retirar el último carrito con portabebés");
                System.out.println("----------------------------------------\n");

                posicion = CarritoPortaBebes.retirar();
            } else if (tipoCarrito == 3) {

                System.out.println("Retirar el último carrito pequeño");
                System.out.println("----------------------------------------\n");

                posicion = CarritoPequeno.retirar();
            }
        
            if (posicion != -1) {
                System.out.println("----------------------------------------");
                System.out.println("El carrito estaba en la posición número: " + (posicion + 1));
                System.out.println("----------------------------------------\n");
            }

                System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                respuesta = leer.next().charAt(0);
                leer.nextLine();
                if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                    System.out.print("ERROR! solo puedes ingresar (S/N)");
                    do {
                        System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                        respuesta = leer.next().charAt(0);
                        leer.nextLine();
                    } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
                }
            
        } while (respuesta == 'S' || respuesta == 's');
    }

    public static void ingresarCarrito(int tipoCarrito) {
        String codigo;
        char respuesta;
        int posicion = -1;
    
        do {
            clear();
            System.out.println("--Ingresar un nuevo carrito--");
            System.out.print("\nIngrese el código del carrito: ");
            codigo = leer.next();
    
            if (tipoCarrito == 1) {
                if (CarritoNormal.numElementos() < 10) {
                    CarritoNormal.agregar(codigo);
                    posicion = CarritoNormal.posicionn();
                } else {
                    System.out.println("No se pueden agregar más de 10 carritos normales.");
                }
            } else if (tipoCarrito == 2) {
                if (CarritoPortaBebes.numElementos() < 5) {
                    CarritoPortaBebes.agregar(codigo);
                    posicion = CarritoPortaBebes.posicionn();
                } else {
                    System.out.println("No se pueden agregar más de 5 carritos con portabebés.");
                }
            } else if (tipoCarrito == 3) {
                if (CarritoPequeno.numElementos() < 5) {
                    CarritoPequeno.agregar(codigo);
                    posicion = CarritoPequeno.posicionn();
                } else {
                    System.out.println("No se pueden agregar más de 5 carritos pequeños.");
                }
            }
    
                if (posicion != -1) {
                    clear();
                    System.out.println("El carrito se ha agregado en la posición: " + (posicion + 1));
                }

                System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                respuesta = leer.next().charAt(0);
                leer.nextLine();
                if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                    System.out.print("ERROR! solo puedes ingresar (S/N)");
                    do {
                        System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
                        respuesta = leer.next().charAt(0);
                        leer.nextLine();
                    } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
                }
            
        } while (respuesta == 'S' || respuesta == 's');
    }
    

    public static void clear() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void Enter() {
        System.out.print("Presione Enter para continuar...");
        continuar.nextLine();
    }

    public static int menuTipoCarrito() {
        int opcion = 0;
        do {
            clear();
            System.out.println("\nSeleccione el tipo de carrito que desee ");
            System.out.println("--------------------------------------");
            System.out.println("1. Carrito Normal");
            System.out.println("2. Carrito con Portabebés");
            System.out.println("3. Carrito Pequeño");
            System.out.println("4. Salir");
            System.out.print("\nSeleccione una opción: ");
    
            if (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Seleccione una opción con número entero.");
                leer.nextLine();
                continue; 
            }
    
            opcion = leer.nextInt();
    
        } while (opcion < 1 || opcion > 4);
    
        return opcion;
    }
    

    public static int menuGestion() {
        int opcion = 0;
        do {
            clear();
            System.out.println("\nGestión de Carritos ");
            System.out.println("--------------------------------------");
            System.out.println("1. Ingresar Carrito");
            System.out.println("2. Retirar Carrito");
            System.out.println("3. Mostrar Carritos Totales");
            System.out.println("4. Regresar");
            System.out.print("\nSeleccione una opción: ");

            if (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Seleccione una opción con número entero.");
                leer.nextLine();
                continue; 
            }

            opcion = leer.nextInt();

        } while (opcion < 1 || opcion > 4);
        return opcion;
    }

} // App
