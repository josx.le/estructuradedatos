/*PRÁCTICA 1. FACTORIAL */


/*Nombre: López Espinoza José */

/*Fecha: 15 de Enero del 2024 */

/*Cuatrimestre: 4to Cuatrimestre */

/*Grupo: C */


import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        
        Scanner leer = new Scanner(System.in);
        int num;

        do {
            System.out.print("Introduce un número entero que seá > 0 y <= 10: ");
            num = leer.nextInt();
        }while(num <= 0 || num > 10);

        double a = 1;
        for(int i = 1; i <= num; i++) { 
            double factorial = a * i;
            double b = factorial(i);
            System.out.printf("%d! * %d = %.0f %n", (int) a, i, factorial);
            a = b;
        }
        
         leer.close();
    }//main

    public static double factorial (double numero) {
        double result = 0;
        if(numero == 0)
            result = 1;
        else
        result = numero * factorial(numero - 1);
        return result;
    }
}//class
