package lista_enlazada;

public class Nodo {

    Object dato;
    Nodo enlace;

    public Nodo (Object objeto) {
        dato = objeto;
        enlace = null;
    }

    public Nodo (Object objeto, Nodo nodo) {
        dato = objeto;
        enlace = nodo;
    }

    public Object getDato(){
        return dato;
    }

    public Nodo getEnlace() {
        return enlace;
    }

    public void setEnlace(Nodo nodo) {
        this.enlace = nodo;
    }
    
}// Nodo
