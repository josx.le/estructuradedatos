/*-------------------------------------------------------------------------------------------------- */

// Alumno: López Espinoza José

// Fecha: 24 de febrero del 2024

// Cuatrimestre y Grupo: 4C

/*-------------------------------------------------------------------------------------------------- */

import java.util.*;

public class App {

    static Scanner leer = new Scanner(System.in);
    static List<String> nombres = new ArrayList<>();
    static List<String> SerieOPeli = new ArrayList<>();

    public static void main(String[] args) throws Exception {
    
        inicializarDatos();

        int opc;

        // Code
        do {
            clear();
            mostrarMenu();

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! seleccione una opción con número entero.");
                leer.nextLine();
                mostrarMenu();
            }

            opc = leer.nextInt();
            leer.nextLine();

            switch (opc) {
                case 1:
                    clear();
                    registrar();
                    break;
                case 2:
                    clear();
                    consultarRegistro();
                    break;
                case 3:
                    clear();
                    consultarTodos();
                    break;
                case 4:
                    clear();
                    eliminarRegistro();
                    break;
                case 5:
                    clear();
                    eliminarTodos();
                    break;
                case 6:
                    clear();
                    actualizarRegistro();
                    break;
                case 7:
                    clear();
                    desplegarSublista();
                    break;
                case 8:
                    clear();
                    System.out.println("Bye..");
                    break;
                default:
                    clear();
                    System.out.println("ERROR! eliga la opción que desea (del 1 al 8)");
            }
        } while (opc != 8);

        leer.close();
    }

    public static void clear() {

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }


    public static void mostrarMenu() {
        System.out.println("\nGestión de datos de perros famosos!");
        System.out.println("----------------------------------------");
        System.out.println("1. Registrar");
        System.out.println("2. Consultar un registro");
        System.out.println("3. Consultar todos los registros");
        System.out.println("4. Eliminar un registro");
        System.out.println("5. Eliminar todos los registros");
        System.out.println("6. Actualizar un registro");
        System.out.println("7. Desplegar una sublista");
        System.out.println("8. Salir");
        System.out.print("\nSeleccione una opción: ");
    }

    public static void inicializarDatos() {
        nombres.add("Coraje");
        nombres.add("Scooby Doo");
        nombres.add("Blue");
        nombres.add("Clifford");
        nombres.add("Bolt");
        nombres.add("Dog");
        nombres.add("Bluey");
        nombres.add("Pluto");
        nombres.add("Odie");
        nombres.add("Pulgoso");
        nombres.add("Niebla");
        nombres.add("Ayudante de santa");

        SerieOPeli.add("Coraje, el perro cobarde (Serie)");
        SerieOPeli.add("Scooby-Doo (Serie)");
        SerieOPeli.add("Las pistas de Blue (serie)");
        SerieOPeli.add("Clifford, el gran perro rojo (Serie)");
        SerieOPeli.add("Bolt (Pelicula)");
        SerieOPeli.add("Cat-Dog (Serie)");
        SerieOPeli.add("Bluey (Serie)");
        SerieOPeli.add("La casa de Mickie Mouse (Serie)");
        SerieOPeli.add("Garfield y sus amigos (Serie)");
        SerieOPeli.add("Hanna-Barbera (Serie)");
        SerieOPeli.add("La aventura de Niebla (Pelicula)");
        SerieOPeli.add("Los Simpson (Serie)");

    }

    public static void registrar() {
        char respuesta;
    
        do {
            System.out.print("Ingrese el nombre del perro famoso: ");
    
            while (leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! No se aceptan números como nombre.");
                leer.nextLine();
                System.out.print("Ingrese el nombre del perro famoso: ");
            }
    
            String nombrePerro = leer.nextLine();
    
            System.out.print("Ingrese nombre de la caricatura donde aparece: ");

            while (leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! No se aceptan números como nombre de Pelicula / Serie.");
                leer.nextLine();
                System.out.print("Ingrese nombre de la caricatura donde aparece: ");
            }

            String caricatura = leer.nextLine();
    
            nombres.add(nombrePerro);
            SerieOPeli.add(caricatura);
    
            System.out.println("Se ha agregado el nombre del perro junto con su nombre de película o serie :D");
    
            System.out.print("¿Desea registrar otro perro? (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();
        } while (respuesta == 'S' || respuesta == 's');
    }
    
    public static void consultarRegistro() {
        char respuesta;
        do{
        System.out.print("Ingrese el nombre del perro a consultar: ");
        String nombrePerro = leer.nextLine();

        int indice = nombres.indexOf(nombrePerro);

        if (indice != -1) {
            String caricatura = SerieOPeli.get(indice);
            clear();
            System.out.println("Caricatura donde aparece " + nombrePerro + ": " + caricatura);
        } else {
            System.out.println("No se encontró el perro en los registros.");
        }
        System.out.print("¿Desea consultar otro perro? (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();
        } while (respuesta == 'S' || respuesta == 's');

    }

    public static void consultarTodos() {
        System.out.println("\t\tLista de registros\n");
        System.out.printf("%-20s %-30s\n", "Nombre del perro", "Caricatura");
        System.out.println("----------------------------------------");

        for (int i = 0; i < nombres.size(); i++) {
            System.out.printf("%-2d %-20s %-30s\n", (i + 1), nombres.get(i), SerieOPeli.get(i));
        }

        System.out.print("\nPresione cualquier botón para regresar: ");
        leer.nextLine();
 

    }

    public static void eliminarRegistro() {
        char respuesta;
    
        do {
            clear();
            System.out.println("\t\tEliminación de registros\n");
            System.out.printf("%-20s %-30s\n", "Nombre del perro", "Caricatura");
            System.out.println("----------------------------------------");
    
            for (int i = 0; i < nombres.size(); i++) {
                System.out.printf("%-2d %-20s %-30s\n", (i + 1), nombres.get(i), SerieOPeli.get(i));
            }
    
            System.out.print("\nIngrese el número del registro que desea eliminar: ");
    
            // Invertir la condición del bucle
            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el número del registro que desea eliminar: ");
            }
    
            int numeroRegistro = leer.nextInt();
            leer.nextLine();
    
            if (numeroRegistro >= 1 && numeroRegistro <= nombres.size()) {
                int indice = numeroRegistro - 1;
    
                System.out.println(nombres.get(indice) + " eliminado exitosamente.");
    
                nombres.remove(indice);
                SerieOPeli.remove(indice);
    
            } else {
                System.out.println("Número de registro no válido. Ingrese un número dentro del rango de registros.");
            }
    
            System.out.print("\n¿Desea eliminar otro perro de la lista? (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();
        } while (respuesta == 'S' || respuesta == 's');
    }
    
    public static void eliminarTodos() {
            System.out.println("\nNombre del perro\t\tCaricatura");
            System.out.println("----------------------------------------");
            for(int i = 0; i < nombres.size(); i++){
                System.out.println((i + 1) + " " + nombres.get(i) + "\t\t" + SerieOPeli.get(i));
            }
            System.out.print("\n¿Está seguro de que desea eliminar todos los registros? (S/N): ");
            char respuesta = leer.next().charAt(0);

            if (respuesta == 'S' || respuesta == 's') {
                nombres.clear();
                SerieOPeli.clear();
                System.out.println("Todos los registros de la lista han sido eliminados.");
            } else {
                System.out.println("Operación cancelada.");
            }
    }

    public static void actualizarRegistro() {
        char respuesta;
        do{
            System.out.println("\t\tActualización de registros\n");
            System.out.printf("%-20s %-30s\n", "Nombre del perro", "Caricatura");
            System.out.println("----------------------------------------");

            for (int i = 0; i < nombres.size(); i++) {
                System.out.printf("%-2d %-20s %-30s\n", (i + 1), nombres.get(i), SerieOPeli.get(i));
            }
            System.out.print("\nIngrese el número del perro que desea actualizar: ");

            while (!leer.hasNextInt()) {
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el número del perro que desea actualizar: ");
            }

            int numeroRegistro = leer.nextInt();
            leer.nextLine(); 
        
            if (numeroRegistro >= 1 && numeroRegistro <= nombres.size()) {
                int indice = numeroRegistro - 1; 
                
                clear();
                System.out.println("¿Qué desea actualizar de " + nombres.get(indice) + "?");
                System.out.println("1. Nombre");
                System.out.println("2. Nombre de pelicula / serie");
                System.out.print("Seleccione una opción: ");

                while (!leer.hasNextInt()) {
                    System.out.println("ERROR! Solo se aceptan números.");
                    leer.nextLine();
                    System.out.println("¿Qué desea actualizar de " + nombres.get(indice) + "?");
                    System.out.println("1. Nombre");
                    System.out.println("2. Nombre de pelicula / serie");
                    System.out.print("Seleccione una opción: ");
                }

                int opcion = leer.nextInt();
                leer.nextLine(); 
        
                switch (opcion) {
                    case 1:
                        clear();
                        System.out.print("Ingrese el nuevo nombre del perro: ");

                        while (leer.hasNextInt()) {
                            System.out.println("ERROR! no se aceptan datos de tipo númerico.");
                            leer.nextLine();
                            System.out.print("Ingrese el nuevo nombre del perro: ");
                        }

                        String nuevoNombre = leer.nextLine();

                        nombres.set(indice, nuevoNombre);
                        System.out.println("Registro actualizado exitosamente.");
                        break;
                    case 2:
                        clear();
                        System.out.print("Ingrese el nombre de la caricatura: ");

                        while (leer.hasNextInt()) {
                            System.out.println("ERROR! no se aceptan datos de tipo númerico.");
                            leer.nextLine();
                            System.out.print("Ingrese el nombre de la caricatura: ");
                        }

                        String nuevaCaricatura = leer.nextLine();
                        SerieOPeli.set(indice, nuevaCaricatura);
                        System.out.println("Registro actualizado exitosamente.");
                        break;
                    default:
                        System.out.println("Opción no válida.");
                }

            } else {
                System.out.println("Número de registro no válido. Por favor, ingrese un número dentro del rango de registros.");
            }

            System.out.print("¿Desea actualizar los datos de otro perro? (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();
        } while (respuesta == 'S' || respuesta == 's');

    }

    public static void desplegarSublista() {
        char respuesta;
        do{
        System.out.print("Ingrese el índice inicial de la sublista: ");

        while (!leer.hasNextInt()) {
            System.out.println("ERROR! Solo se aceptan números.");
            leer.nextLine();
            System.out.print("Ingrese el índice inicial de la sublista: ");
        }

        int pos1 = leer.nextInt();

        System.out.print("Ingrese el índice final de la sublista: ");

        while (!leer.hasNextInt()) {
            System.out.println("ERROR! Solo se aceptan números.");
            leer.nextLine();
            System.out.print("Ingrese el índice final de la sublista: ");
        }

        int pos2 = leer.nextInt();

        if (pos1 >= 0 && pos1 < nombres.size() && pos2 >= pos1 && pos2 < nombres.size()) {
            clear();
            System.out.println("\t\tSubLista\n");
            System.out.printf("%-20s %-30s\n", "Nombre del perro", "Caricatura");
            System.out.println("----------------------------------------");

            for (int i = pos1; i <= pos2; i++) {
                System.out.printf("%-2d %-20s %-30s\n", i , nombres.get(i), SerieOPeli.get(i));
            }

        } else {
            System.out.println("Índices no válidos para la sublista.");
        }

        System.out.print("\n¿Desea hacer otra sublista? (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();
        } while (respuesta == 'S' || respuesta == 's');
    }
}