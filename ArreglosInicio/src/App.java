import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner read = new Scanner(System.in);
        String[] deptos = new String[5];
        // int[] empleados = new int[5];

        System.out.print("\033[H\033[2J");  
        System.out.flush();  

        System.out.println("--Departamentos--");
        System.out.println(" ");
        deptos[0] = "Recursos Humanos";
        deptos[1] = "Contabilidad";
        deptos[2] = "Capacitación";
        deptos[3] = "Producción";
        deptos[4] = "Ventas";


        
        for(int i=0; i < deptos.length; i++) {
            System.out.println(i + 1 + ". " + deptos[i]);
        }

        read.close();
    }/// Main
}// Class
