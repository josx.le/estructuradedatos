/*-------------------------------------------------------------------------------------------------- */

// Alumno: López Espinoza José

// Fecha: 11 de Marzo del 2024

// Cuatrimestre y Grupo: 4C

/*-------------------------------------------------------------------------------------------------- */

import java.util.*;
import lista_enlazada.*;


public class App {

    Scanner leer = new Scanner(System.in);
    Lista lista = new Lista();
    public void main(String[] args) throws Exception {

        int opc;

        do {
            clear();
            mostrarMenu();

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! seleccione una opción con número entero.");
                leer.nextLine();
                mostrarMenu();
            }

            opc = leer.nextInt();
            leer.nextLine();

            switch (opc) {
                case 1:
                    AgregarAlFrente();
                    break;
                case 2:
                    clear();
                    AgregarAlFinal();
                    break;
                case 3:
                    clear();
                    BuscarPorReferencia();
                    break;
                case 4:
                    clear();
                    EliminarPrimerValor();
                    break;
                case 5:
                    clear();
                    EliminarUltimoVaalor();
                    break;
                case 6:
                    clear();
                    EliminarPorReferencia();
                    break;
                case 7:
                    clear();
                    BuscarPorPosicion();
                    break;
                case 8:
                    clear();
                    BuscarXValor();
                    break;
                case 9:
                    clear();
                    ActualizarXValor();
                    break;
                case 10:
                    clear();
                    ActualizarXPosicion();
                    break;
                case 11:
                    clear();
                    listaH();
                    break;
                case 12:
                    clear();
                    listaV();
                    break;
                case 13:
                    clear();
                    CantElemnt();
                    break;
                case 14:
                    clear();
                    System.out.println("Bye..");
                    break;
                default:
                    clear();
                    System.out.println("ERROR! eliga la opción que desea (del 1 al 14)");
            }

        } while (opc != 14);

        leer.close();

    } // Main

    public static void clear() {

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void mostrarMenu() {
        System.out.println("\nLista de opciones: ");
        System.out.println("--------------------------------------");
        System.out.println("1. Agregar un valor al principio");
        System.out.println("2. Agregar un valor al final");
        System.out.println("3. Agregar valor por referencia");
        System.out.println("4. Eliminar el primer valor");
        System.out.println("5. Eliminar el ultimo valor");
        System.out.println("6. Eliminar un valor por referencia");
        System.out.println("7. Buscar un valor por posición");
        System.out.println("8. Saber si un valor existe (Buscar por valor)");
        System.out.println("9. Actualizar buscando por valor");
        System.out.println("10. Actualizar buscando por posición");
        System.out.println("11. Desplegar lista en forma horizontal");
        System.out.println("12. Desplegar lista en forma vertical");
        System.out.println("13. Conocer la cantidad de elementos");
        System.out.println("14. Salir");
        System.out.print("\nSeleccione una opción: ");
    }

    public void AgregarAlFrente() {
        char respuesta;

        do {
            clear();
            System.out.print("Ingrese el valor que desea agregar al principio: ");

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el valor que desea agregar al principio: ");
            }

            int PrimerValor = leer.nextInt();
            leer.nextLine();
            lista.insertarAlFrente(PrimerValor);

            lista.imprimir();
            System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void AgregarAlFinal() {
        char respuesta;

        do{
            clear();
            System.out.print("Ingrese valor al final de la lista: ");

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese valor al final de la lista: ");
            }

            int UltimoValor = leer.nextInt();
            leer.nextLine();
            lista.agregarAlFInal(UltimoValor);
            lista.imprimir();

            System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void BuscarPorReferencia() {
        Object dato;
        char respuesta;
        int nuevoValor;
        clear();

        do{
        System.out.println("Agregar valor por referencia");
            
        System.out.print("\nEscribe el nuevo valor: ");

        while (!leer.hasNextInt()) {
            clear();
            System.out.println("ERROR! Solo se aceptan números.");
            leer.nextLine();
            System.out.print("\nIngrese valor al final de la lista: ");
        }

        nuevoValor = leer.nextInt();
        System.out.print("Agregar despues del dato: ");

        while (!leer.hasNextInt()) {
            clear();
            System.out.println("ERROR! Solo se aceptan números.");
            leer.nextLine();
            System.out.print("\nIngrese valor al final de la lista: ");
        }

        dato = leer.nextInt();
        lista.insertarPorReferencia(dato, nuevoValor);
        lista.imprimir();

        System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void EliminarPrimerValor() {
        char respuestaEliminar;
        char respuestaRepetir;
        
        do {
            clear();
            lista.imprimir();
            System.out.print("\n¿Deseas eliminar el primer valor de la lista? (S/N): ");
            respuestaEliminar = leer.next().charAt(0);
            leer.nextLine(); 

            while (respuestaEliminar != 'S' && respuestaEliminar != 's' && respuestaEliminar != 'N' && respuestaEliminar != 'n') {
                System.out.println("ERROR! solo puedes ingresar (S/N)");
                System.out.print("¿Deseas eliminar el primer valor de la lista? (S/N): ");
                respuestaEliminar = leer.next().charAt(0);
                leer.nextLine(); 
            }

            if (respuestaEliminar == 'S' || respuestaEliminar == 's') {
                try {
                    Object elementoEliminado = lista.eliminarDelFrente();
                    System.out.println("Elemento eliminado: " + elementoEliminado);
                    lista.imprimir();
                } catch (ExxeptionListaVacia e) {
                    System.out.println("Error: La lista está vacía, no se puede eliminar el primer elemento.");
                }
            }

            System.out.print("¿Deseas repetir el proceso? (S/N): ");
            respuestaRepetir = leer.next().charAt(0);
            leer.nextLine();

            while (respuestaRepetir != 'S' && respuestaRepetir != 's' && respuestaRepetir != 'N' && respuestaRepetir != 'n') {
                System.out.println("ERROR! solo puedes ingresar (S/N)");
                System.out.print("¿Deseas repetir el proceso? (S/N): ");
                respuestaRepetir = leer.next().charAt(0);
                leer.nextLine(); 
            }

        } while (respuestaRepetir == 'S' || respuestaRepetir == 's');

    }

    public void EliminarUltimoVaalor() {
        char respuestaEliminar;
        char respuestaRepetir;
        
        do {
            clear();
            lista.imprimir();
            System.out.print("\n¿Deseas eliminar el ultimo valor de la lista? (S/N): ");
            respuestaEliminar = leer.next().charAt(0);
            leer.nextLine(); 

            while (respuestaEliminar != 'S' && respuestaEliminar != 's' && respuestaEliminar != 'N' && respuestaEliminar != 'n') {
                System.out.println("ERROR! solo puedes ingresar (S/N)");
                System.out.print("¿Deseas eliminar el ultimo valor de la lista? (S/N): ");
                respuestaEliminar = leer.next().charAt(0);
                leer.nextLine(); 
            }

            if (respuestaEliminar == 'S' || respuestaEliminar == 's') {
                try {
                    Object elementoEliminado = lista.eliminarDelFinal();
                    System.out.println("Elemento eliminado: " + elementoEliminado);
                    lista.imprimir();
                } catch (ExxeptionListaVacia e) {
                    System.out.println("Error: La lista está vacía, no se puede ultimo el primer elemento.");
                }
            }

            System.out.print("¿Deseas repetir el proceso? (S/N): ");
            respuestaRepetir = leer.next().charAt(0);
            leer.nextLine();

            while (respuestaRepetir != 'S' && respuestaRepetir != 's' && respuestaRepetir != 'N' && respuestaRepetir != 'n') {
                System.out.println("ERROR! solo puedes ingresar (S/N)");
                System.out.print("¿Deseas repetir el proceso? (S/N): ");
                respuestaRepetir = leer.next().charAt(0);
                leer.nextLine(); 
            }

        } while (respuestaRepetir == 'S' || respuestaRepetir == 's');
    }

    public void EliminarPorReferencia() {
        int dato;
        char respuestaRepetir;

        do{
            clear();
            System.out.println("Eliminar por referencia");
            lista.imprimir();
            System.out.print("\nDato a eliminar: ");

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el valor que desea eliminar por referencia: ");
            }

            dato = leer.nextInt();
            lista.eliminarPorReferencia(dato);
            lista.imprimir();

            System.out.print("¿Deseas repetir el proceso? (S/N): ");
            respuestaRepetir = leer.next().charAt(0);
            leer.nextLine();

            while (respuestaRepetir != 'S' && respuestaRepetir != 's' && respuestaRepetir != 'N' && respuestaRepetir != 'n') {
                System.out.println("ERROR! solo puedes ingresar (S/N)");
                System.out.print("¿Deseas repetir el proceso? (S/N): ");
                respuestaRepetir = leer.next().charAt(0);
                leer.nextLine(); 
            }
        } while (respuestaRepetir == 'S' || respuestaRepetir == 's');

    }

    public void BuscarPorPosicion() {
        char respuesta;

        do{
        clear();
        lista.imprimir();

        System.out.print("\nIngrese la posición que desea buscar: ");
        while (!leer.hasNextInt()) {
            clear();
            lista.imprimir();
            System.out.println("ERROR! Solo se aceptan números.");
            leer.nextLine();
            System.out.print("\nIngrese la posición que desea buscar: ");
        }

        int posicion = leer.nextInt();
        leer.nextLine();

        Object resultado = lista.buscarPorPosicion(posicion);

        if (resultado != null) {
            System.out.println("El elemento en la posición " + posicion + " es: " + resultado);
        } else {
            System.out.println("No hay elemento en la posición " + posicion + ".");
        }

        System.out.print("\n¿Deseas buscar otro valor por posición?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas agregar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void BuscarXValor() {
        char respuesta;

        do {
            clear();

            System.out.print("\nIngrese el valor que desea buscar: ");
            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el valor que desea buscar: ");
            }

            int valor = leer.nextInt();
            leer.nextLine();

            Object resultado = lista.buscarPorValor(valor);

            if (resultado != null) {
                System.out.println("El elemento " + valor + " si existe");
            } else {
                System.out.println("El elemento " + valor + " no existe");
            }

            System.out.print("\n¿Deseas buscar otro valor por valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas buscar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');
    }

    public void ActualizarXValor() {
        char respuesta;
        do{
            clear();
            lista.imprimir();

            System.out.print("Ingresa el valor que quieres actualizar: ");

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el valor que desea buscar: ");
            }

            int valorV = leer.nextInt();
            System.out.print("Ingresa el nuevo valor: ");

            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el nuevo valor: ");
            }

            int valorN = leer.nextInt();

            lista.actualizarPorValor(valorV, valorN);
            lista.imprimir();

            System.out.print("\n¿Deseas buscar otro valor por valor?  (S/N): ");
                respuesta = leer.next().charAt(0);
                leer.nextLine();

                if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                    System.out.print("ERROR! solo puedes ingresar (S/N)");
                    do {
                        System.out.print("\n¿Deseas actualizar otro valor?  (S/N): ");
                        respuesta = leer.next().charAt(0);
                        leer.nextLine();
                    } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
                }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void ActualizarXPosicion() {
        char respuesta;
        do{
            clear();
            lista.imprimir();
        
            System.out.print("Ingresa la posición que quieres actualizar: ");
        
            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese la posición que desea actualizar: ");
            }
        
            int posicion = leer.nextInt();
            System.out.print("Ingresa el nuevo valor: ");
        
            while (!leer.hasNextInt()) {
                clear();
                System.out.println("ERROR! Solo se aceptan números.");
                leer.nextLine();
                System.out.print("\nIngrese el nuevo valor: ");
            }
        
            int valorN = leer.nextInt();
        
            lista.actualizarPorPosicion(posicion, valorN);
            lista.imprimir();

            System.out.print("\n¿Deseas buscar otro valor por valor?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                    do {
                        System.out.print("\n¿Deseas actualizar otro valor?  (S/N): ");
                        respuesta = leer.next().charAt(0);
                        leer.nextLine();
                    } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
                }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void listaH() {
        char respuesta;
        do{
            lista.imprimir();

            System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                    do {
                        System.out.print("\n¿Deseas actualizar otro valor?  (S/N): ");
                        respuesta = leer.next().charAt(0);
                        leer.nextLine();
                    } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
                }
        } while (respuesta == 'S' || respuesta == 's');

    }

    public void listaV() {
        char respuesta;

        do {
            lista.imprimirVertical();

            System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas actualizar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');
    }

    public void CantElemnt() {
        char respuesta;

        do {
            int cantidadElementos = lista.obtenerCantidadElementos();
            System.out.println("La cantidad de elementos en la lista es: " + cantidadElementos);

            System.out.print("\n¿Deseas repetir el proceso?  (S/N): ");
            respuesta = leer.next().charAt(0);
            leer.nextLine();

            if (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n') {
                System.out.print("ERROR! solo puedes ingresar (S/N)");
                do {
                    System.out.print("\n¿Deseas actualizar otro valor?  (S/N): ");
                    respuesta = leer.next().charAt(0);
                    leer.nextLine();
                } while (respuesta != 'S' && respuesta != 's' && respuesta != 'N' && respuesta != 'n');
            }
        } while (respuesta == 'S' || respuesta == 's');
    }
    
} // App
