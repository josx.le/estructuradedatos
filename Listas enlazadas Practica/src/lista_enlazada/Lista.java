package lista_enlazada;

public class Lista {
    private Nodo primerNodo;
    private Nodo ultimoNodo;
    private String nombre;

    // Se crea el constructor con la lista 

    public Lista() {
        nombre = "Lista";
    }

    public Lista(String nombreLista) {
        nombre = nombreLista;
        primerNodo = ultimoNodo = null;
    }

    // Determinar si la lista está vacía 

    public boolean estaVacia() {
        return primerNodo == null;
    }

    // Buscar por valor

    public Nodo buscarPorValor(Object destino) {

        Nodo indice;

        for(indice = primerNodo; indice != null; indice = indice.enlace) {

            if(destino.equals(indice.dato)) 
                return indice;
        }
        return null;

    }

    // Imprimir

    public void imprimir() {
        if(estaVacia()) {
            System.out.println("\nLista vacía, nada que imprimir");
        }
        else{
            System.out.print("La lista es: ");
            Nodo actual = primerNodo;

            // Mientras no sea el final de la lista, se imprime los datos del nodo actual

            while (actual != null) {
                System.out.printf("%s ", actual.dato);
                actual = actual.enlace;
            }
            System.out.println("");
        }
    } 

    // InsertarAlFrente

    public void insertarAlFrente(Object dato) {
        if(estaVacia()) {
            primerNodo = ultimoNodo = new Nodo(dato);
        }
        else{
            primerNodo = new Nodo(dato, primerNodo);
        }
    }

    // Agregar valores al final de la lista

    public void agregarAlFInal(Object dato) {
        if(estaVacia()) {
            primerNodo = ultimoNodo = new Nodo(dato);
        }
        else {
            ultimoNodo = ultimoNodo.enlace = new Nodo(dato);
        }

    }

    // Insertar por referencia

    public void insertarPorReferencia(Object referencia, Object nuevoValor) {
        Nodo nuevo, anterior;
        anterior = buscarPorValor(referencia);
        if(anterior != null) {
            nuevo = new Nodo(nuevoValor);
            nuevo.enlace = anterior.enlace;
            anterior.enlace = nuevo;
        }
        else {
            System.out.println("La referencia " + referencia + " no esta en la lista");
        }
    }

    // Eliminar el primer elémento

    public Object eliminarDelFrente() throws ExxeptionListaVacia {
        if(estaVacia()) {
            throw new ExxeptionListaVacia(nombre);
        }

        Object elementoEliminado = primerNodo.dato;

        if(primerNodo == ultimoNodo) {
            primerNodo = ultimoNodo = null;
        }
        else {
            primerNodo = primerNodo.enlace;
        }
        return elementoEliminado;

    }

    // Eliminar ultimo elemento de la lista

    public Object eliminarDelFinal() throws ExxeptionListaVacia {
        if(estaVacia()) {
            throw new ExxeptionListaVacia(nombre);
        }

        Object elementoEliminado = ultimoNodo.dato;

        if(primerNodo == ultimoNodo) {
            primerNodo = ultimoNodo = null;
        }
        else{
            Nodo actual = primerNodo;
            while (actual.enlace != ultimoNodo) {
                actual = actual.enlace;
            }

            ultimoNodo = actual;
            actual.enlace = null;
        }

        return elementoEliminado;

    }

    // Eliminar Por Referencia

    public void eliminarPorReferencia(Object referencia) {
        if(estaVacia()) {
            System.out.println("La lista vacía, nada que eliminar. ");
        }
        else{
            if(buscarPorValor(referencia) != null) {
                if(referencia.equals(primerNodo.dato)) {
                    primerNodo = primerNodo.enlace;
                }
                else{
                    Nodo actual, siguiente;
                    actual = primerNodo;
                    siguiente = primerNodo.enlace;
                    while ((siguiente.dato != referencia) && siguiente!= null) {
                        siguiente = siguiente.enlace;
                        actual = actual.enlace;
                    }
                    if(siguiente != null) {
                        actual.enlace = siguiente.enlace;
                    }
                    else{
                        ultimoNodo = actual;
                        siguiente = null;
                    }
                }
            }
            else{
                System.out.println("El valor " + referencia + " no se encontro en la lista");
            }
        }// Esta vaía

    }

    // Contar elementos

    public int contarElementos() {
        int cont = 0;
        Nodo indice;
        for(indice = primerNodo; indice != null; indice = indice.enlace)
        cont++;
        return cont;
    }

    // Buscar por posición

    public Object buscarPorPosicion(int posicion) {
        Nodo indice;
        if(posicion < 1 || posicion > contarElementos()) {
            return null;
        }
        else{
            indice = primerNodo;
            for(int i = 1; (i < posicion) && (indice != null); i++) {
                indice = indice.enlace;
            }
            return indice.dato;
        }
    }

    // Actualizar por valor
    
    public void actualizarPorValor(Object valorBuscado, Object nuevoValor) {
        Nodo nodo = buscarPorValor(valorBuscado);

        if (nodo != null) {
            nodo.dato = nuevoValor;
            System.out.println("Valor actualizado");
        } else {
            System.out.println("No se encontró el valor en la lista");
        }
    }

    // Actualizar por posición

    public void actualizarPorPosicion(int posicion, Object nuevoValor) {
        Nodo nodoActual = primerNodo;
        int contador = 1;
    
        while (nodoActual != null && contador < posicion) {
            nodoActual = nodoActual.enlace;
            contador++;
        }
    
        if (nodoActual != null) {
            nodoActual.dato = nuevoValor;
        } else {
            System.out.println("La posición no existe en la lista");
        }
    }

    // Imprimir en vertical

    public void imprimirVertical() {
        Nodo nodoActual = primerNodo;
        while (nodoActual != null) {
            System.out.println(nodoActual.getDato());
            nodoActual = nodoActual.getEnlace();
        }
    }

    // Obtener la cantidad de elementos de la lista

    public int obtenerCantidadElementos() {
        return contarElementos();
    }

}// Lista
