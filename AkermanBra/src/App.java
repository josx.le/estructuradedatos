/* Nombre: López Espinoza José*/

/* Fecha: 19 de enero del 2024 */

/* Cuatrimestre: 4to Cuatrimestre */

/*Grupo: C */


import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        Scanner leer = new Scanner(System.in);
        
        int m, n;
        char repetir;
        App ackerman = new App();

        do {
            m = validacion("m", leer);
            n = validacion("n", leer);

            int resultado = ackerman.formulas(m, n);
            System.out.println("Ackermann  (" + m + ", " + n + ") = " + resultado);

            System.out.print("¿Quieres repetir el programa? (S/N): ");
            repetir = leer.next().charAt(0);
        } while (repetir == 'S' || repetir == 's');

        leer.close();
    }

    public int formulas(int m, int n) {
        if (m == 0) {
            return n + 1;
        } 
        else {
            if (m > 0 && n == 0) {
                return formulas(m - 1, 1);
            }
            else {
                if (m > 0 && n > 0) {
                    return formulas(m - 1, formulas(m, n - 1));
                }
                else {
                    return 0;
                }
            }
        }
    }

    public static int validacion(String mYn, Scanner leer) {
        int respuesta2;
        do {
            System.out.print("Ingrese un número entero para " + mYn + " (entre 0 y 3): ");
            while (!leer.hasNextInt()) {
                System.out.println(" ");
                System.out.println("ERROR!, INGRESE UN NÚMERO ENTERO ENTRE 1 Y 3.");
                leer.next();
            }
            respuesta2 = leer.nextInt();
            if (respuesta2 < 0 || respuesta2 > 3) {
                System.out.println(" ");
                System.out.println("ERROR!, INGRESE UN NÚMERO ENTERO ENTRE 1 Y 3.");
                System.out.println(" ");
            }
        } while (respuesta2 < 0 || respuesta2 > 3);
        return respuesta2;
    }
}
