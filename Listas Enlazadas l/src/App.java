import lista_enlazada.*;
import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner read = new Scanner(System.in);
        Lista lista = new Lista();
        Object dato;
        int nuevoValor;

        // Insertar valores al frente y al final de la lista

        System.out.println("\nAgregando valores");
        lista.insertarAlFrente(-1);
        lista.imprimir();
        lista.agregarAlFInal(0);
        lista.imprimir();
        lista.agregarAlFInal(1);
        lista.imprimir();
        lista.insertarAlFrente(100);
        lista.imprimir();
        lista.insertarAlFrente(-15);
        lista.imprimir();
        lista.agregarAlFInal(41);
        lista.imprimir();
        lista.insertarAlFrente(12);
        lista.imprimir();

        /* Buscar un elemento por valor*/

        // System.out.println("\nBuscar un elemento por valor ");
        // System.out.print("Valor a buscar: ");
        // dato = read.nextInt();
        // if(lista.buscarPorValor(dato) != null) {
        //     System.out.println("El dato " + dato + " si existe");
        // }
        // else{ 
        //     System.out.println("El dato " + dato + " No existe");
        // }

            /*Agregar valor por referencia */

            // System.out.println("\nAgregar valor por referencia");
            
            // System.out.print("Escribe el nuevo valor: ");
            // nuevoValor = read.nextInt();
            // System.out.print("Agregar despues del dato: ");
            // dato = read.nextInt();
            // lista.insertarPorReferencia(dato, nuevoValor);
            // lista.imprimir();

            // Elimina datos de la lista

            // System.out.println("\nEliminar elementos de uno en uno");
            // try {
            //     Object objetoEliminado = lista.eliminarDelFrente();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            //     objetoEliminado = lista.eliminarDelFinal();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            //     objetoEliminado = lista.eliminarDelFinal();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            //     objetoEliminado = lista.eliminarDelFrente();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            //     objetoEliminado = lista.eliminarDelFrente();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            //     objetoEliminado = lista.eliminarDelFinal();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            //     objetoEliminado = lista.eliminarDelFrente();
            //     System.out.printf("Se elimino: %s%n", objetoEliminado);
            //     lista.imprimir();

            // }catch(ExxeptionListaVacia e) {
            //     e.printStackTrace();
            // }

            // Eliminar por referencia 

            System.out.println("\nEliminar por referencia");
            System.out.print("Dato a eliminar: ");
            dato = read.nextInt();
            lista.eliminarPorReferencia(dato);
            lista.imprimir();

        read.close();

    } // Main

}// Class