package lista_enlazada;

public class Lista {
    private Nodo primerNodo;
    private Nodo ultimoNodo;
    private String nombre;

    // Se crea el constructor con la lista 

    public Lista() {
        nombre = "Lista";
    }

    public Lista(String nombreLista) {
        nombre = nombreLista;
        primerNodo = ultimoNodo = null;
    }

    // Agregar valoers al frente de la lista

    public void insertarAlFrente(Object dato) {
        if(estaVacia()) {
            primerNodo = ultimoNodo = new Nodo(dato);
        }
        else{
            primerNodo = new Nodo(dato, primerNodo);
        }
    } // InsertarAlFrente

    // Agregar valores al final de la lista

    public void agregarAlFInal(Object dato) {
        if(estaVacia()) {
            primerNodo = ultimoNodo = new Nodo(dato);
        }
        else {
            ultimoNodo = ultimoNodo.enlace = new Nodo(dato);
        }

    }// Agregar al final

    //Agregar valores entre dos nodos

    public void insertarPorReferencia(Object referencia, Object nuevoValor) {
        Nodo nuevo, anterior;
        anterior = buscarPorValor(referencia);
        if(anterior != null) {
            nuevo = new Nodo(nuevoValor);
            nuevo.enlace = anterior.enlace;
            anterior.enlace = nuevo;
        }
        else {
            System.out.println("La referencia " + referencia + " no esta en la lista");
        }
    }// Insertar por referencia

    //Eliminar el primer elemento

    public Object eliminarDelFrente() throws ExxeptionListaVacia {
        if(estaVacia()) {
            throw new ExxeptionListaVacia(nombre);
        }

        Object elementoEliminado = primerNodo.dato;

        if(primerNodo == ultimoNodo) {
            primerNodo = ultimoNodo = null;
        }
        else {
            primerNodo = primerNodo.enlace;
        }
        return elementoEliminado;

    } // Eliminar del frente

    // Eliminar el ultimo elemento de la lista

    public Object eliminarDelFinal() throws ExxeptionListaVacia {
        if(estaVacia()) {
            throw new ExxeptionListaVacia(nombre);
        }

        Object elementoEliminado = ultimoNodo.dato;

        if(primerNodo == ultimoNodo) {
            primerNodo = ultimoNodo = null;
        }
        else{
            Nodo actual = primerNodo;
            while (actual.enlace != ultimoNodo) {
                actual = actual.enlace;
            }

            ultimoNodo = actual;
            actual.enlace = null;
        }

        return elementoEliminado;

    }// Eliminar al final

    //Eliminar un nodo en medio de la lista

    public void eliminarPorReferencia(Object referencia) {
        if(estaVacia()) {
            System.out.println("Lista vacía, nada que eliminar. ");
        }
        else{
            if(buscarPorValor(referencia) != null) {
                if(referencia.equals(primerNodo.dato)) {
                    primerNodo = primerNodo.enlace;
                }
                else{
                    Nodo actual, siguiente;
                    actual = primerNodo;
                    siguiente = primerNodo.enlace;
                    while ((siguiente.dato != referencia) && siguiente!= null) {
                        siguiente = siguiente.enlace;
                        actual = actual.enlace;
                    }
                    if(siguiente != null) {
                        actual.enlace = siguiente.enlace;
                    }
                    else{
                        ultimoNodo = actual;
                        siguiente = null;
                    }
                }
            }
            else{
                System.out.println("El valor " + referencia + " no se encontro en la lista");
            }
        }// Esta vaía

    }// eliminarPorReferencia


    // Buscar por valores

    public Nodo buscarPorValor(Object destino) {

        Nodo indice;

        for(indice = primerNodo; indice != null; indice = indice.enlace) {

            if(destino.equals(indice.dato)) 
                return indice;
        }
        return null;

    }// Buscar por valor
    
    // Desplegar valores

    public void imprimir() {
        if(estaVacia()) {
            System.out.println("\nLista vacía, nada que imprimir");
        }
        else{
            System.out.print("La lista es: ");
            Nodo actual = primerNodo;

            // Mientras no sea el final de la lista, se imprime los datos del nodo actual

            while (actual != null) {
                System.out.printf("%s ", actual.dato);
                actual = actual.enlace;
            }
            System.out.println("");
        }
    } // Imprimir

    // Determinar si la lista está vacía 

    public boolean estaVacia() {
        return primerNodo == null;
    }

}// Lista
