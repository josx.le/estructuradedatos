import java.util.*;
import lista.*;

public class App {

    static Scanner read = new Scanner(System.in);
    static Scanner continuar = new Scanner(System.in);

    static Pila pilaProd1 = new Pila();
    static Pila pilaProd2 = new Pila();

    public static void main(String[] args) throws Exception {     
        
        int opcionProd, opcionGestion;

        do {
            opcionProd = MenuProductos();

            if(opcionProd != 3)
            do {

                opcionGestion = MenuGestion();

                switch (opcionGestion) {
                    case 1:
                        agregar(opcionProd);
                        break;
                    case 2:
                        retirar(opcionProd);
                        break;
                    case 3:
                        ultimoPaquete(opcionProd);
                        break;
                    case 4:
                        imprimir(opcionProd);
                        break;
                    case 5:
                        totalPaquetes(opcionProd);
                        break;
                    case 6:
                        paquetesPermitidos();
                        break;
                    case 7:
                        System.out.println("Bye..");
                        break;
                }

            }while(opcionGestion != 7);

        }while(opcionProd != 3);
        
        read.close();
        continuar.close();

    } // Main

    public static void ultimoPaquete(int numProducto) {
        clear();
        System.out.println("Información del ultimo paquete del producto " + numProducto);
        if(numProducto == 1) {
            pilaProd1.valorCima();
        }
        else{
            pilaProd2.valorCima();
        }
        enter();
    }

    public static void totalPaquetes(int numProducto) {
        clear();
        System.out.println("Total de paquetes actuales");
        if(numProducto == 1) {
            System.out.println("Cantidad: " + pilaProd1.numElementos());
        }
        else {
            System.out.println("Cantidad: " + pilaProd2.numElementos());

        }
        enter();
    }

    public static void paquetesPermitidos() {
        clear();
        System.out.println("Paquetes permitidos");
        System.out.println("Número máximo: " + pilaProd1.maxElementos());
    }

    public static void retirar(int numProducto) {
        clear();
        System.out.println("Retirar el ultimo paquete");
        if(numProducto == 1) {
            pilaProd1.retirar();
        }
        else {
            pilaProd2.retirar();
        }
        enter();
    }


    public static void imprimir(int numProducto) {
        clear();
        System.out.println("Lista de paquetes del producto " + numProducto);
        if(numProducto == 1)
            pilaProd1.imprimir();
        else
            pilaProd2.imprimir();
        enter();
    }


    public static void agregar(int numProducto) {
        String codigo, descripcion;

        clear();
        System.out.println("Agregar paquetes del producto " + numProducto);
        System.out.println("Agregar los datos");
        System.out.print("Código: ");
        codigo = read.next();
        System.out.print("Descripción: ");
        descripcion = read.next();
        if(numProducto == 1) {
            pilaProd1.agregar(codigo, descripcion);
        }
        else {
            pilaProd2.agregar(codigo, descripcion);
        }
        System.out.println("Se agrego el paquete.\n");
        enter();
    }

    public static void enter() {
        continuar.useDelimiter("\n");
        System.out.print("Presione ENTER para continuar...");
        continuar.next();
    }

    public static int MenuProductos() {
        int opcion = 0;
        do {

            clear();
            System.out.println("\nLista de opciones: ");
            System.out.println("--------------------------------------");
            System.out.println("1. Producto(1)");
            System.out.println("2. Producto(2)");
            System.out.println("3. Salir");
            System.out.print("\nSeleccione una opción: ");
            opcion = read.nextInt();

        }while(opcion < 1 || opcion > 3);
        return opcion;
    }

    public static int MenuGestion() {
        int opcion = 0;
        do {

            clear();
            System.out.println("\nLista de opciones: ");
            System.out.println("--------------------------------------");
            System.out.println("1. Agregar paquete");
            System.out.println("2. Eliminar paquete");
            System.out.println("3. Consultar el último paquete");
            System.out.println("4. Consultar todos los paquetes");
            System.out.println("5. Total de paquetes actuales");
            System.out.println("6. Máximo de paquetes permitidos");
            System.out.println("7. Regresar");
            System.out.print("\nSeleccione una opción: ");
            opcion = read.nextInt();

        }while(opcion < 1 || opcion > 7);
        return opcion;

    }

    public static void clear() {

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

} // App
