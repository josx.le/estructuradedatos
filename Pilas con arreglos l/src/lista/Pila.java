package lista;

public class Pila {
    int max = 4;
    int cima = 0;
    Producto[] productos = new Producto[max];

    // Iniciarlizar el arreglo de objetos

    public Pila() {
        for(int i = 0; i < max; i++) {
            productos[i] = new Producto();
        }
    }

    // Cantidad máxima de elementos que se pueden agregar

    public int maxElementos() {
        return max;
    }

    // Cantidad de elementos agregados

    public int numElementos() {
        return cima;
    }

    // Saber si la pila esta llena 

    public boolean pilaLlena() {
        return cima >= max;
    }

    // Saber si la pila esta vacía 

    public boolean pilaVacia() {
        return cima == 0;
    }

    // Agregar paquetes a la pila

    public void agregar(String codigo, String descripcion) {
        if(pilaLlena()) {
            System.out.println("Estiba máxima, no se puede agregar más paquetes");
        }
        else {
            productos[cima].setCodigo(codigo);
            productos[cima].setDescripcion(descripcion);
            cima++;
        }
    }

    // Imprimir la lista del arreglo

    public void imprimir() {
        if(pilaVacia()) {
            System.out.println("No hay paquetes que imprimir.");
        }
        else {
            System.out.println("Código \t Descripción");
            System.out.println("------------------------------------------------------");
            for(int i = 0; i < cima; i++) {
                System.out.print(productos[i].getCodigo());
                System.out.print("\t" + productos[i].getDescripcion());
            }
        }
    }

    // Retirar un paquete de la cima

    public void retirar() {
        if(pilaVacia()) {
            System.out.println("No hay paquetes para retirar");
        }
        else{
            cima --;
            System.out.println("Paquete retirado:");
            System.out.println("Código: " + productos[cima].getCodigo());
            System.out.println("Descripción: " + productos[cima].getDescripcion());

            productos[cima].setCodigo(null);
            productos[cima].setDescripcion(null);

        }
    }

    // Valor de la cima

    public void valorCima() {
        if (pilaVacia()) {
            System.out.println("No hay paquetes para mostrar");
        }
        else {
            System.out.println("Código: " + productos[cima - 1].getCodigo());
            System.out.println("Descripción: " + productos[cima - 1].getDescripcion());
        }
    }
}