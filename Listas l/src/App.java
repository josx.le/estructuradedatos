import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner read = new Scanner(System.in);
        Scanner readString = new Scanner(System.in);
        readString.useDelimiter("\n");
        int tam, num, pos1, pos2;
        String perro;

        // Crear la lista

        List<String> nombres = new ArrayList<String>();

        // Agregar valores

        nombres.add("Coraje");
        nombres.add("Scooby Doo");
        nombres.add("Blue");
        nombres.add("Clifford");
        nombres.add("Bolt");
        nombres.add("Dog");
        nombres.add("Bluey");
        nombres.add("Pluto");
        nombres.add("Odie");
        nombres.add("Pulgoso");
        nombres.add("Niebla");
        nombres.add("Ayudante de santa");

        clear();

        //Formas de desplegar los valores 

        System.out.println("\n--Perros famosos de caricaturas--");
        System.out.println("----------------------------------------------------------------------------------------------------");

        //Forma 1

        System.out.println("\n Listas de perros famosos (tipo array) \n");

        System.out.println(nombres);

        //Forma 2

        System.out.println("\n-------------------------------------------------------------------------------------------------");

        System.out.println("\n Listas de perros famosos (tipo lista) \n");
        
        for(int i = 0; i < nombres.size(); i++){
            System.out.println("\t" + (i + 1) + " " + nombres.get(i));
        }

        System.out.println("-------------------------------------------------------------------------------------------------");

        //Obtener el tamaño de la lista

        tam = nombres.size();

        System.out.println("La lista tiene " + tam + " elementos");

        //Obtener un elemento

        System.out.println("------------------------------------------------------------------------------------------------------");

        System.out.println("Obtener un elemento\n");

        do{
            System.out.print("Ingresa un número entre 1 y " + tam + ": ");
            num = read.nextInt();
        }while(num < 1 || num > tam);

        System.out.println("El perro famoso es: " + nombres.get(num - 1));

        System.out.println("-------------------------------------------------------------------------------------------------------");
        
        // Formas de eliminar un elemento

        // Forma 1 por medio del índice

        System.out.println("\nEliminar un elemento");

        do{
            System.out.print("Ingresa un número entre 1 y " + tam + ": ");
            num = read.nextInt();
        }while(num < 1 || num > tam);

        perro = nombres.get(num-1);
        System.out.println("Nombre a eliminar: " + perro);
        nombres.remove(num - 1);

        clear();

        System.out.println("El nombre: " + perro + "se ha eliminado");

        System.out.println("\n Listas de perros famosos (tipo lista) \n");
        
        for(int i = 0; i < nombres.size(); i++){
            System.out.println("\t" + (i + 1) + " " + nombres.get(i));
        }

        // Forma 2 por medio del nombre

        System.out.println("-------------------------------------------------------------------------------------------------------");

        System.out.println("\nEliminar un elemento (Forma 2)");

        System.out.print("Ingresa el nombre del perro famoso: ");
        perro = readString.next();

        if(nombres.contains(perro)){
            nombres.remove(perro);

            clear();

            System.out.println("El nombre " + perro + " se ha eliminado");

            System.out.println("\n Listas de perros famosos (tipo lista) \n");
        
            for(int i = 0; i < nombres.size(); i++){
                System.out.println("\t" + (i + 1) + " " + nombres.get(i));
            }

        }
        else {
            System.out.println("El nombre " + perro + " no esta en la lista");
        }

        System.out.println("-------------------------------------------------------------------------------------------------------");

        //Modificar un valor

        System.out.println("Modificar un nombre\n");

        do{
            System.out.print("Ingresa un número entre 1 y " + tam + ": ");
            num = read.nextInt();
        }while(num < 1 || num > tam);

        perro = nombres.get(num - 1);
        System.out.println("Nombre a modificar: " + perro);

        System.out.print("Escriba el nuevo nombre: ");
        perro = readString.next();
        nombres.set(num - 1, perro);

        clear();

        System.out.println("El dato se actualizo a " + perro);

        System.out.println("\n Listas de perros famosos (tipo lista) \n");
        
        for(int i = 0; i < nombres.size(); i++){
            System.out.println("\t" + (i + 1) + " " + nombres.get(i));
        }

        System.out.println("-------------------------------------------------------------------------------------------------------");


        // Extrar una sublista 

        clear();

        System.out.println("Extraer lista");
        System.out.print("Ingresa los números de los perros famosos (Deben estar entre 1 y "  + tam + "): ");

        do{
            System.out.print("Primer número: ");
            pos1 = read.nextInt();
        }while(pos1 < 1 || pos1 > tam);

        do{
            System.out.print("Segundo número: ");
            pos2 = read.nextInt();
        }while(pos2 < pos1 || pos2 > tam);

        System.out.println("La sublista es: ");
        System.out.println(nombres.subList(pos1 - 1, pos2));

        System.out.println("-------------------------------------------------------------------------------------------------------");

        // Saber si la lista esta vacía

        System.out.println("¿Lista vacía?");
        if(nombres.isEmpty()){
            System.out.println("La lista está vacía ");
        }
        else{
            System.out.println("La lista todavía tiene valores");
        }

        // Eliminar todos los elementos de la lista

        System.out.println("Todos los elementos serán eliminados ");
        nombres.clear();

        if(nombres.isEmpty()){
            System.out.println("La lista está vacía ");
        }
        else{
            System.out.println("La lista todavía tiene valores");
        }

        read.close();
        readString.close();

    }// Main

    public static void clear() {

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}// Class
