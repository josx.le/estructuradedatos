/*Nombre: López Espinoza José */

/*Fecha: 29 de enero del 2024 */

/*Cuatrimestre: 4to */

/*Grupo: C */

/*7. Vectores en columnas */

/*Leer dos vectores de tipo numérico entero con 10 valores cada uno y determinar:

a. la suma,
b. promedio,
c. producto y
d. número mayor de cada elemento coincidente en cada vector.

Desplegar los resultados en columnas. Utilizar mínimo tres ciclos: uno para leer los valores,
otro para realizar las operaciones y el tercero para desplegar los resultados. */

import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        //Variables

        Scanner leer = new Scanner(System.in);
        int[] vector1 = new int[10];
        int[] vector2 = new int[10];
        int suma, producto, NumMayorDeCadaElemnt;
        String repetir;

        //Print's

        clear();

        System.out.println("Ingrese los valores del primer vector\n");
        for (int i = 0; i < 10; i++) {
            System.out.print("Valor " + (i + 1) + ": ");
            vector1[i] = validacion(leer);

        }

        clear();

        System.out.println("Ingrese los valores del segundo vector\n");
        for (int i = 0; i < 10; i++) {
            System.out.print("Valor " + (i + 1) + ": ");
            vector2[i] = validacion(leer);

        }

        clear();

        System.out.printf("%-15s%-15s%-15s%-15s%-15s%-15s%-15s%n", "Elemento","Vector 1", "Vector 2", "Suma", "Promedio", "Producto", "Número Mayor");
        for (int i = 0; i < 10; i++) {
            suma = vector1[i] + vector2[i];
            producto = vector1[i] * vector2[i];
            NumMayorDeCadaElemnt = Math.max(vector1[i], vector2[i]);

            double promedio = (double) suma / 2;

            System.out.printf("%-15d%-15s%-15s%-15d%-15.2f%-15d%-15d%n", i + 1, vector1[i], vector2[i], suma, promedio, producto, NumMayorDeCadaElemnt);
        }

        System.out.print("\n Desea repetir el programa (S/N): ");
        repetir = leer.next();

        if (repetir.equalsIgnoreCase("S")) {
            main(args);
        }

        else {
            System.out.println("Fin del Programa!");
            leer.close();
        }


        

    }//Main

    public static void clear() {

        System.out.print("\033[H\033[2J");  
        System.out.flush();
    }

     public static int validacion(Scanner scanner) {
        while (true) {
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                
                scanner.nextLine();
                System.out.println("ERROR! INGRESE UN NÚMERO ENTERO.\n");
                System.out.print("Vuelva a ingresar el valor: ");
            }
        }
    }

}//Class
