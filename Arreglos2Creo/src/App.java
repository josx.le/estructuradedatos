import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner read = new Scanner(System.in);
        String [] empleados;
        float [] sueldos;
        int cant;
        char respuestatsueldo = 's';
        
        clear();

        System.out.println("Sueldos de empleados \n");

        System.out.print("¿Cuantos empleados son?: ");
        cant = read.nextInt();

        empleados = new String[cant];
        sueldos = new float[cant];

        clear();

        System.out.print("Ingrese los datos \n");
        for(int i = 0; i < cant; i++) {
            System.out.print("\nIngrese el nombre del empleado número: " + (i + 1) + ": ");
            empleados[i] = read.next();

            System.out.print("Sueldo: ");
            sueldos[i] = read.nextFloat();
        }

        clear();

        System.out.println("Listas de empleados \n");

        System.out.println("Num \t Nombre \t Sueldo");

        for(int i = 0; i < cant; i++) {
            System.out.printf(i + 1 + "\t %-10s %.0f %n", empleados[i], sueldos[i]);
        }

        do {
            System.out.print("\n Desea actualizar algún saldo (S/N): ");
            respuestatsueldo = read.next().charAt(0);
        }while(Character.toUpperCase(respuestatsueldo) !='S' && Character.toUpperCase(respuestatsueldo) !='N' );

        if (respuestatsueldo == 'S' || respuestatsueldo == 's') {

            clear();

            System.out.print("\n Eliga el número del empleado: ");
            int numempleadorespuesta = read.nextInt();
            //verificar el valor ingresado se encuentre en el rango de empleados

            if(numempleadorespuesta < 1 || numempleadorespuesta > cant ) {
                System.out.println("EL EMPLEADO NO EXISTE!");
            }

            else {
                System.out.println("Datos del empleado: " + numempleadorespuesta);
                System.out.println("Nombre: " + empleados[numempleadorespuesta - 1]);
                System.out.println("Sueldo: " + sueldos[numempleadorespuesta - 1]);

                do {
                    System.out.print("\n Actualizar algún saldo (S/N): ");
                    respuestatsueldo = read.next().charAt(0);
                }while(Character.toUpperCase(respuestatsueldo) !='S' && Character.toUpperCase(respuestatsueldo) !='N' );
                if (respuestatsueldo == 'S' || respuestatsueldo == 's') {

                    clear();

                    sueldos[numempleadorespuesta - 1] = sueldos[numempleadorespuesta - 1] * (float)1.10;
                    System.out.println("Sueldo actualizado!");
                    System.out.println("Nombre: " + empleados[numempleadorespuesta - 1]);
                    System.out.println("Sueldo: " + sueldos[numempleadorespuesta - 1]);
                }
                else{
                    System.out.println("Fin del programa!");
                    read.close();
                }

            }

        }
        else{
            System.out.println("Fin del programa!");
            read.close();
        }

    }// Main

    public static void clear() {

        System.out.print("\033[H\033[2J");  
        System.out.flush();
    }

}// Class




//si es no se acaba el programa

//si es si le pedimos los datos del usuario

//si el usuario no existe